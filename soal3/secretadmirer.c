#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <sys/stat.h>
#include <stdlib.h>

static  const  char *dirpath = "/home/wardaas/Documents/sisop4-3/inifolderetc/sisop";

struct secret_path {
        char secret_path[1000];
        char path[1000];
        struct stat st;
};

struct secret_path secret_paths[5000];
int num = 0;

struct secret_path {
        char secret_path[1000];
        char path[1000];
        struct stat st;
};

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        //printf("Error : Failed to open the file: %s\n", filename);
        return;
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Error : Memory allocation failed\n");
        fclose(file);
        return;
    }
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
       // printf("Error : Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        //printf("Error : Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }
    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }
    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    //printf("Encoding completed successfully. file: %s\n", filename);
}

static int initializing(const char* scrtPath, const char* actPath){
        DIR *dp;
        struct dirent* de;
        struct stat st;
        char fullpath[2000];
        char fullscrpath[2000];

        dp = opendir(actPath);
        if(dp == NULL) return -errno;
        while((de = readdir(dp)) != NULL){
                if((strcmp(de->d_name, ".") == 0) || (!strcmp(de->d_name, ".."))) continue;
                sprintf(fullpath, "%s/%s", actPath, de->d_name);

                if(lstat(fullpath, &st) == -1){
                        perror("Unable to find path or directory");
                        continue;
                }

                char tempName[1000];

                if(S_ISDIR(st.st_mode)){
                        strcpy(tempName, de->d_name);
                        char *p = tempName;
                        while(*p != '\0'){
                                *p = toupper(*p);
                                p++;
                        }

                        if(strlen(tempName) <= 4){
                                char encName[1000];
                                int div;
                                size_t idx = 0;
                                for(int i = 0; i < strlen(tempName); i++){
                                        div = 128; //8th bit
                                        while(div){
                                                if ((tempName[i] & div) == div) encName[idx] = '1';
                                                else encName[idx] = '0';
                                                idx++;
                                                if(div == 1 && i < strlen(tempName)-1){
                                                        encName[idx] = ' ';
                                                        idx++;
                                                }
                                                div /= 2;
                                        }
                                }
                                encName[idx] = '\0';
                                strcpy(tempName, encName);
                        }
                        else if(tempName[0] == 'l' || tempName[0] == 'L' || tempName[0] == 'u' || tempName[0] == 'U' || tempName[0] == 't' || tempName[0] == 'T' || tempName[0] == 'h' || tempName[0] == 'H'){
                                encodeBase64File(tempName);
                        }

                        sprintf(fullscrpath, "%s/%s", scrtPath, tempName);
                        strcpy(secret_paths[num].secret_path, fullscrpath);
                        strcpy(secret_paths[num].path, fullpath);
                        secret_paths[num].st = st;
                        num++;

                        initializing(fullscrpath, fullpath);
                }
                else {
                        strcpy(tempName, de->d_name);
                        char *p = tempName;
                        while(*p != '\0'){
                                *p = tolower(*p);
                                p++;
                        }

                        if(strlen(tempName) <= 4){
                                char encName[1000];
                                int div;
                                size_t idx = 0;
                                for(int i = 0; i < strlen(tempName); i++){
                                        div = 128; //8th bit
                                        while(div){
                                                if ((tempName[i] & div) == div) encName[idx] = '1';
                                                else encName[idx] = '0';
                                                idx++;
                                                if(div == 1 && i < strlen(tempName)-1){
                                                        encName[idx] = ' ';
                                                        idx++;
                                                }
                                                div /= 2;
                                        }
                                }
                                encName[idx] = '\0';
                                strcpy(tempName, encName);
                        }
                        else if(tempName[0] == 'l' || tempName[0] == 'L' || tempName[0] == 'u' || tempName[0] == 'U' || tempName[0] == 't' || tempName[0] == 'T' || tempName[0] == 'h' || tempName[0] == 'H'){
                                encodeBase64File(tempName);
                        }
                        sprintf(fullscrpath, "%s/%s", scrtPath, tempName);
                        strcpy(secret_paths[num].secret_path, fullscrpath);
                        strcpy(secret_paths[num].path, fullpath);
                        secret_paths[num].st = st;
                        num++;
                }
        }
        return 0;
}

static void* secret_init(struct fuse_conn_info *conn){
        (void) conn;
        num = 0;
        char actPath[1000];
        char scrPath[1000];

        strcpy(actPath, dirpath);
        strcpy(scrPath, "");
        strcpy(secret_paths[num].secret_path, "/");

        int sign = initializing(scrPath, dirpath);
        if(sign != 0) perror("Unable to initialize");
        return NULL;
}

static  int  secret_getattr(const char *path, struct stat *stbuf) {
        struct secret_path currPath;

        if((strcmp(path, "/") == 0) || (strcmp(path, ".") == 0) || (strcmp(path, "..") == 0)){
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
                return 0;
        }

        for(int i = 0; i < num; i++){
                currPath = secret_paths[i];
                if(strcmp(currPath.secret_path, path) == 0) {
                        *stbuf = currPath.st;
                        return 0;
                }
        }

        return -errno;
}
static int secret_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
        char final_path[1000];

        sprintf(final_path, "%s%s",dirpath,path);

        int res = 0;

        struct secret_path currPath;

        int index = 0;
        int found = 0;

        if(strcmp(path, "/") == 0) found = 1;
        else {
                for(int i = 0; i < num; i++){
                        currPath = secret_paths[i];
                        if(strcmp(currPath.secret_path, path) == 0){
                                index = i;
                                found = 1;
                                break;
                        }
                }
        }

        if(!found) return -errno;

        filler(buf, ".", NULL, 0);
        filler(buf, "..", NULL, 0);

        for(int i = index+1; i < num; i++){
                currPath = secret_paths[i];
                if(strstr(currPath.secret_path, path) != NULL){
                        int valid = 1;
                        for(int i = strlen(path); currPath.secret_path[i] != '\0'; i++){
                                if(currPath.secret_path[i] == '/' && i != strlen(path)){
                                        valid = 0;
                                        break;
                                }
                        }
                        if(!valid) continue;
                        struct stat st = currPath.st;
                        res = (filler(buf, strrchr(currPath.secret_path, '/') + 1, &st, 0));
                        //continue;
                }
        }

        return 0;
}



static int secret_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char final_path[1000];
    if(strcmp(path,"/") == 0){
        path=dirpath;

        sprintf(final_path,"%s",path);
    }
    else sprintf(final_path, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(final_path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int secret_open(const char *path, struct fuse_file_info *fi){
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char password[100];
            printf("Input Password: ");
            fflush(stdout);
            fgets(password, sizeof(password), stdin);

            size_t len = strlen(password);
            if (len > 0 && password[len - 1] == '\n')
                password[len - 1] = '\0';

            while (strcmp(password, pw) != 0) {
                printf("Incorret Password, Try Again!.\nInput password: ");
                fflush(stdout);
                fgets(password, sizeof(password), stdin);

                len = strlen(password);
                if (len > 0 && password[len - 1] == '\n')
                    password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int secret_mkdir(const char *path, mode_t mode){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int secret_rename(const char *from, const char *to){
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static struct fuse_operations secret_oper = {
    .getattr = secret_getattr,
    .readdir = secret_readdir,
    .read = secret_read,
    .init = secret_init,
    .mkdir = secret_mkdir,
    .rename = secret_rename,
    .open = secret_open,
};



int  main(int  argc, char *argv[]){
    umask(0);

    return fuse_main(argc, argv, &secret_oper, NULL);
}
