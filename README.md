## Anggota Kelompok
1. Wardatul Amalia Safitri (5025211006)
2. Nur Azizah (5025211252)
3. Hana Maheswari (5025211182

## Soal 1
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran.

Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.

Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya.

b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.

Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?

Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

Catatan: 
- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
- Perhatikan port  pada masing-masing instance.

## Penyelesaian soal 1

Nomor 1 diminta untuk membuat sebuah file bernama storage.c yang akan mendownload sebuah dataset dari kaggle dan meng-unzip file yang sudah terdownload. Kemudian file .csv tersebut akan dicetak melalui command berikut ini.
```c
int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    printPlayer();
    return 0;
}
```

Command pertama adalah untuk mendownload file database, dan yang selanjutnya adalah untuk meng-unzip. Lalu file .csv itu akan disimpan dalam struct bernama Player, lalu dia akan diprint satu persatu. Setiap field dalam struct ini merepresentasikan atribut dari pemain, seperti nama, umur, foto, kewarganegaraan, overall rating, potensi, klub, dan sebagainya.

```c
typedef struct {
    int ID;
    char Name[50];
    int Age;
    char Photo[200];
    char Nationality[100];
    char Flag[200];
    int Overall;
    int Potential;
    char Club[200];
    char ClubLogo[200];
    char Value[50];
    char Wage[50];
    int Special;
    char PreferredFoot[50];
    float InternationalReputation;
    float WeakFoot;
    float SkillMoves;
    char WorkRate[100];
    char BodyType[100];
    char RealFace[100];
    char position[100];
    char Joined[100];
    char LoanedFrom[100];
    int ContractValidUntil;
    char Height[10];
    char Weight[10];
    char ReleaseClause[20];
    float KitNumber;
    char BestOverallRating[10];
} Player;

```

Selanjutnya void 'printPlayer' memiliki fungsi untuk membuka dan mencetak isi dari file "FIFA23_official_data.csv". Fungsi ini akan membuka file "FIFA23_official_data.csv" dalam mode "r" (read) melalui Command 'FILE *file = fopen("FIFA23_official_data.csv", "r");'. Fungsi 'fopen()' mengembalikan pointer ke file yang telah berhasil dibuka, yang kemudian disimpan dalam variabel 'file' dengan tipe 'FILE *'.

```c
void printPlayer() {
    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

```

Lalu kita membaca data pemain sepak bola dari file "FIFA23_official_data.csv" dengan menggunakan fungsi 'strtok()' untuk membagi string line menjadi token-token yang dipisahkan oleh koma. Setiap token kemudian diperiksa dan nilainya disimpan ke dalam variabel pemain yang sesuai, yaitu 'player'. 4. Setiap case dalam switch case akan memproses token dan menyimpannya ke dalam field yang sesuai dalam struct 'player'. Misalnya, pada case 0, token akan diubah menjadi bilangan integer menggunakan fungsi 'atoi()' dan disimpan dalam field 'player.ID'. Pada case 1, token akan disalin ke dalam field 'player.Name' menggunakan fungsi 'strncpy()'. Setelah pemrosesan token pada case tertentu selesai, baris 'token = strtok(NULL, ",");' digunakan untuk memperoleh token selanjutnya dari string 'line'. Pemanggilan 'strtok(NULL, ",")' dengan argumen NULL menandakan bahwa kita ingin melanjutkan pemrosesan pada string yang sama dan mencari token selanjutnya. Token selanjutnya akan disimpan kembali dalam variabel 'token'. Selanjutnya, variabel 'field' akan diincrement untuk mengarahkan pemrosesan ke case berikutnya dalam switch case.

Pada intinya, bagian ini melakukan pemrosesan token-token dalam string line menggunakan fungsi 'strtok()' dan menyimpan token-token tersebut dalam field-field yang sesuai dalam struct player, sehingga memungkinkan pemrosesan data pemain dari file CSV dan penyimpanan data pemain dalam struct yang sesuai untuk digunakan.

```bash
  char *token = strtok(line, ",");
        int field = 0;
        while (token != NULL) {
            switch (field) {
                case 0:
                    player.ID = atoi(token);
                    break;
                case 1:
                    strncpy(player.Name, token, sizeof(player.Name));
                    break;
                case 2:
                    player.Age = atoi(token);
                    break;
                case 3:
                    strncpy(player.Photo, token, sizeof(player.Photo));
                    break;
                case 4:
                    strncpy(player.Nationality, token, sizeof(player.Nationality));
                    break;
                case 5:
                    strncpy(player.Flag, token, sizeof(player.Flag));
                    break;
                case 6:
                    player.Overall = atoi(token);
                    break;
                case 7:
                    player.Potential = atoi(token);
                    break;
                case 8:
                    strncpy(player.Club, token, sizeof(player.Club));
                    break;
                case 9:
                    strncpy(player.ClubLogo, token, sizeof(player.ClubLogo));
                    break;
                case 10:
                    strncpy(player.Value, token, sizeof(player.Value));
                    break;
                case 11:
                    strncpy(player.Wage, token, sizeof(player.Wage));
                    break;
                case 12:
                    player.Special = atoi(token);
                    break;
                case 13:
                    strncpy(player.PreferredFoot, token, sizeof(player.PreferredFoot));
                    break;
                case 14:
                    player.InternationalReputation = atof(token);
                    break;
                case 15:
                    player.WeakFoot = atof(token);
                    break;
                case 16:
                    player.SkillMoves = atof(token);
                    break;
                case 17:
                    strncpy(player.WorkRate, token, sizeof(player.WorkRate));
                    break;
                case 18:
                    strncpy(player.BodyType, token, sizeof(player.BodyType));
                    break;
                case 19:
                    strncpy(player.RealFace, token, sizeof(player.RealFace));
                    break;
                case 20:
                    strncpy(player.position, token, sizeof(player.position));
                    break;
                case 21:
                    strncpy(player.Joined, token, sizeof(player.Joined));
                    break;
                case 22:
                    strncpy(player.LoanedFrom, token, sizeof(player.LoanedFrom));
                    break;
                case 23:
                    player.ContractValidUntil = atoi(token);
                    break;
                case 24:
                    strncpy(player.Height, token, sizeof(player.Height));
                    break;
                case 25:
                    strncpy(player.Weight, token, sizeof(player.Weight));
                    break;
                case 26:
                    strncpy(player.ReleaseClause, token, sizeof(player.ReleaseClause));
                    break;
                case 27:
                    player.KitNumber = atof(token);
                    break;
                case 28:
                    strncpy(player.BestOverallRating, token, sizeof(player.BestOverallRating));
                    break;
            }

            token = strtok(NULL, ",");
            field++;
        }

```

Selanjutnya kita akan mencetak informasi pemain yang memenuhi beberapa kriteria tertentu. Kriteria untuk dipenuhi adalah:


- player.Age < 25 -> usia pemain kurang dari 25 tahun.

- player.Potential > 85 -> potensi pemain lebih dari 85.

- strcmp(player.Club, "Manchester City") != 0 -> pemain bukan berasal dari klub "Manchester City".

Lalu kita mengincrement variabel num untuk melacak jumlah pemain yang memenuhi kriteria tersebut, kemudian mencetak pemain yang memenuhi kriteria menggunakan 'printf'.

```c
 if (player.Age < 25 && player.Potential > 85 && strcmp(player.Club, "Manchester City") != 0) {
            num++;
            printf("Player %d\n", num);
            printf("Name: %s\n", player.Name);
            printf("Club: %s\n", player.Club);
            printf("Age: %d\n", player.Age);
            printf("Potential: %d\n", player.Potential);
            printf("Photo URL: %s\n", player.Photo);
            printf("\n");
        }
    }

    fclose(file);

}

```
Kemudian kita membuat sebuah Dockerfile, Docker Image, dan Docker Container. Berikut adalah bagian dari Dockerfile-nya, yang akan mem-build image dengan command docker build -t storage-app . Setelah berhasil, Docker Image tersebut kita run sebagai container. Kemudian Docker Image ini dipublish ke Docker Hub.

```
FROM gcc:latest

WORKDIR /app

COPY storage.c .
COPY FIFA23_official_data.csv .

RUN gcc -o storage storage.c

ENTRYPOINT ["./storage"]

```

Terakhir kita membuat instance sebanyak 5 menggunakan Docker Compose. Kita buat folder bernama 'Barcelona' dan 'Napoli' yang diisi dengan 'docker-compose.yml'. Berikut adalah isi dari 'docker-compose.yml'.

```
version: '3'
services:
  app:
    image: raesnandess/sisop-repo
    deploy:
      replicas: 5

```

Image-nya berasal dari Docker Hub tadi, dan replicanya kita set menjadi 5. Kemudian kita cek apakah instancenya sudah berjalan menggunakan perintah docker logs.

## Soal 3

a. Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 

Struktur dasar dari kode FUSE termodifikasi yang kami buat adalah sebagai berikut
```bash
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <sys/stat.h>
#include <stdlib.h>

static  const  char *dirpath = "/home/wardaas/Documents/sisop4-3/inifolderetc/sisop";


static struct fuse_operations secret_oper = {
    .getattr = secret_getattr,
    .readdir = secret_readdir,
    .read = secret_read,
    .init = secret_init,
    .mkdir = secret_mkdir,
    .rename = secret_rename,
    .open = secret_open,
};



int  main(int  argc, char *argv[]){
    umask(0);

    return fuse_main(argc, argv, &secret_oper, NULL);
}
```
Dalam FUSE termodifikasi ini source mountnya adalah inifolderetc/sisop dan menerapkan fungsi untuk mendapatkan atribut (getattr), membaca directory (readdir), membaca file (read), menginisialisasi directory dalam FUSE (init), membuat direktory baru (mkdir), dan membuka sebuah file (open).


b. Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).
```bash
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        //printf("Error : Failed to open the file: %s\n", filename);
        return;
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Error : Memory allocation failed\n");
        fclose(file);
        return;
    }
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
       // printf("Error : Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        //printf("Error : Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }
    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }
    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    //printf("Encoding completed successfully. file: %s\n", filename);
}
```

c. Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

Dalam tahap ini, pertama kami akan mendapatkan atribut dari sebuah directory menggunakan fungsi berikut
```bash
static  int  secret_getattr(const char *path, struct stat *stbuf) {
        struct secret_path currPath;

        if((strcmp(path, "/") == 0) || (strcmp(path, ".") == 0) || (strcmp(path, "..") == 0)){
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
                return 0;
        }

        for(int i = 0; i < num; i++){
                currPath = secret_paths[i];
                if(strcmp(currPath.secret_path, path) == 0) {
                        *stbuf = currPath.st;
                        return 0;
                }
        }

        return -errno;
}
```
Fungsi ini akan sekaligus mengidentifikasi apakah sebuah atribut adalah directory atau bukan. Jika atribut tersebut adalah direktori, maka st_mode akan diubah menjadi S_IFDIR.

Selanjutnya, akan dilakukan pembacaan direktori dan initialisasi nama direktori dan file dalam FUSE
```bash
static int initializing(const char* scrtPath, const char* actPath){
        DIR *dp;
        struct dirent* de;
        struct stat st;
        char fullpath[2000];
        char fullscrpath[2000];

        dp = opendir(actPath);
        if(dp == NULL) return -errno;
        while((de = readdir(dp)) != NULL){
                if((strcmp(de->d_name, ".") == 0) || (!strcmp(de->d_name, ".."))) continue;
                sprintf(fullpath, "%s/%s", actPath, de->d_name);

                if(lstat(fullpath, &st) == -1){
                        perror("Unable to find path or directory");
                        continue;
                }

                char tempName[1000];

                if(S_ISDIR(st.st_mode)){
                        strcpy(tempName, de->d_name);
                        char *p = tempName;
                        while(*p != '\0'){
                                *p = toupper(*p);
                                p++;
                        }

                        if(strlen(tempName) <= 4){
                                char encName[1000];
                                int div;
                                size_t idx = 0;
                                for(int i = 0; i < strlen(tempName); i++){
                                        div = 128; //8th bit
                                        while(div){
                                                if ((tempName[i] & div) == div) encName[idx] = '1';
                                                else encName[idx] = '0';
                                                idx++;
                                                if(div == 1 && i < strlen(tempName)-1){
                                                        encName[idx] = ' ';
                                                        idx++;
                                                }
                                                div /= 2;
                                        }
                                }
                                encName[idx] = '\0';
                                strcpy(tempName, encName);
                        }
                        else if(tempName[0] == 'l' || tempName[0] == 'L' || tempName[0] == 'u' || tempName[0] == 'U' || tempName[0] == 't' || tempName[0] == 'T' || tempName[0] == 'h' || tempName[0] == 'H'){
                                encodeBase64File(tempName);
                        }

                        sprintf(fullscrpath, "%s/%s", scrtPath, tempName);
                        strcpy(secret_paths[num].secret_path, fullscrpath);
                        strcpy(secret_paths[num].path, fullpath);
                        secret_paths[num].st = st;
                        num++;

                        initializing(fullscrpath, fullpath);
                }
                else {
                        strcpy(tempName, de->d_name);
                        char *p = tempName;
                        while(*p != '\0'){
                                *p = tolower(*p);
                                p++;
                        }

                        if(strlen(tempName) <= 4){
                                char encName[1000];
                                int div;
                                size_t idx = 0;
                                for(int i = 0; i < strlen(tempName); i++){
                                        div = 128; //8th bit
                                        while(div){
                                                if ((tempName[i] & div) == div) encName[idx] = '1';
                                                else encName[idx] = '0';
                                                idx++;
                                                if(div == 1 && i < strlen(tempName)-1){
                                                        encName[idx] = ' ';
                                                        idx++;
                                                }
                                                div /= 2;
                                        }
                                }
                                encName[idx] = '\0';
                                strcpy(tempName, encName);
                        }
                        else if(tempName[0] == 'l' || tempName[0] == 'L' || tempName[0] == 'u' || tempName[0] == 'U' || tempName[0] == 't' || tempName[0] == 'T' || tempName[0] == 'h' || tempName[0] == 'H'){
                                encodeBase64File(tempName);
                        }
                        sprintf(fullscrpath, "%s/%s", scrtPath, tempName);
                        strcpy(secret_paths[num].secret_path, fullscrpath);
                        strcpy(secret_paths[num].path, fullpath);
                        secret_paths[num].st = st;
                        num++;
                }
        }
        return 0;
}

static void* secret_init(struct fuse_conn_info *conn){
        (void) conn;
        num = 0;
        char actPath[1000];
        char scrPath[1000];

        strcpy(actPath, dirpath);
        strcpy(scrPath, "");
        strcpy(secret_paths[num].secret_path, "/");

        int sign = initializing(scrPath, dirpath);
        if(sign != 0) perror("Unable to initialize");
        return NULL;
}

static  int  secret_getattr(const char *path, struct stat *stbuf) {
        struct secret_path currPath;

        if((strcmp(path, "/") == 0) || (strcmp(path, ".") == 0) || (strcmp(path, "..") == 0)){
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
                return 0;
        }

        for(int i = 0; i < num; i++){
                currPath = secret_paths[i];
                if(strcmp(currPath.secret_path, path) == 0) {
                        *stbuf = currPath.st;
                        return 0;
                }
        }

        return -errno;
}
static int secret_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
        char final_path[1000];

        sprintf(final_path, "%s%s",dirpath,path);

        int res = 0;

        struct secret_path currPath;

        int index = 0;
        int found = 0;

        if(strcmp(path, "/") == 0) found = 1;
        else {
                for(int i = 0; i < num; i++){
                        currPath = secret_paths[i];
                        if(strcmp(currPath.secret_path, path) == 0){
                                index = i;
                                found = 1;
                                break;
                        }
                }
        }

        if(!found) return -errno;

        filler(buf, ".", NULL, 0);
        filler(buf, "..", NULL, 0);

        for(int i = index+1; i < num; i++){
                currPath = secret_paths[i];
                if(strstr(currPath.secret_path, path) != NULL){
                        int valid = 1;
                        for(int i = strlen(path); currPath.secret_path[i] != '\0'; i++){
                                if(currPath.secret_path[i] == '/' && i != strlen(path)){
                                        valid = 0;
                                        break;
                                }
                        }
                        if(!valid) continue;
                        struct stat st = currPath.st;
                        res = (filler(buf, strrchr(currPath.secret_path, '/') + 1, &st, 0));
                        //continue;
                }
        }

        return 0;
}
```
Jika st_mode nya menyatakan atribut tersebut adalah direktori, maka semua karakter namanya akan diubah menjadi uppercase menggunakan command toupper. 
```bash
if(S_ISDIR(st.st_mode)){
    strcpy(tempName, de->d_name);
    char *p = tempName;
    while(*p != '\0'){
        *p = toupper(*p);
        p++;
    }
```
Sedangkan jika st_mode nya bukan direktori, artinya atribut tersebut adalah file yang namanya harus diubah menjadi lowercase dengan command tolower. 
```bash
strcpy(tempName, de->d_name);
char *p = tempName;
while(*p != '\0'){
*p = tolower(*p);
p++;
```
Selain itu, jika namanya kurang dari atau sama dengan 4 huruf, maka setiap karakternya akan diubah menjadi kode biner dari ASCII karakter tersebut. 
```bash
if(strlen(tempName) <= 4){
    char encName[1000];
    int div;
    size_t idx = 0;
    for(int i = 0; i < strlen(tempName); i++){
        div = 128; //8th bit
        while(div){
            if ((tempName[i] & div) == div) encName[idx] = '1';
            else encName[idx] = '0';
            idx++;
            if(div == 1 && i < strlen(tempName)-1){
                encName[idx] = ' ';
                idx++;
            }
            div /= 2;
        }
    }
    encName[idx] = '\0';
    strcpy(tempName, encName);
}
```
Hasil output
![Screenshot_2023-06-03_193711](/uploads/86d3556a8d6a503dc8258bd633258efe/Screenshot_2023-06-03_193711.png)

d. Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
e. Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 
Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

## Soal 4
a. Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
b. Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
```bash
int isModularDir(const char *dirName){
    const char *prefix = "module_";
    return strncmp(dirName, prefix, strlen(prefix)) == 0;
}
```
Pada fungsi ini akan dicek direktori mana yang berawalan "module_".

c. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
```bash
void logSysCall(const char *level, const char *command, const char *desc1, const char *desc2){
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", t);

    FILE *logFile = fopen(logFilePath, "a");
    if(logFile == NULL){
        perror("Error access log file");
        return;
    }
    if(desc2 != NULL)
        fprintf(logFile, "%s::%s::%s::%s::%s\n", level, timestamp, command, desc1, desc2);
    else
        fprintf(logFile, "%s::%s::%s::%s\n", level, timestamp, command, desc1);
    fclose(logFile);
}
```
Fungsi ini akan mendapatkan waktu lalu menyesuaikan formatnya menjadi "%y%m%d-%H:%M:%S". Kemudian file log dibuka dan dicetak sesuai dengan kondisi desc2 level, timestamp, command, desc.

d. Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
```bash
void FileChunk(const char *filePath){
    FILE *inputFile = fopen(filePath, "rb");
    if(inputFile == NULL){
        perror("Error open file");
        return;
    }
    char chunk[CHUNK_SIZE];
    size_t bytesRead;
    int chunkIndex = 0;

    while ((bytesRead = fread(chunk, 1, sizeof(chunk), inputFile)) > 0){
        char newFileName[PATH_MAX];
        snprintf(newFileName, sizeof(newFileName), "%s.%03d", filePath, chunkIndex);
        FILE *outputFile = fopen(newFileName, "wb");
        if(outputFile == NULL){
            perror("Error create file");
            break;
        }
        fwrite(chunk, 1, bytesRead, outputFile);
        fclose(outputFile);
        chunkIndex++;
    }
    fclose(inputFile);
}
```
Menerima argumen filePath, file dibuka kemudian dibaca. bytesRead akan menyimpanjumlah byte setiap fread dipanggil. Jika bytesRead > 0, maka masih ada data yang harus dibaca. Selanjutnya dibuat file baru dengan indeks chunk. File dibuka lalu data dalan chunk ditulis pada file output dan indeks chunk ditambah 1.
```bash
void mergeChunksToFile(const char *filePath){
    FILE *outputFile = fopen(filePath, "wb");
    if(outputFile == NULL){
        perror("Error creating file");
        return;
    }
    int chunkIndex = 0;
    char chunkFileName[PATH_MAX];
    snprintf(chunkFileName, sizeof(chunkFileName), "%s.%03d", filePath, chunkIndex);
    
    while(access(chunkFileName, F_OK) == 0){
        FILE *chunkFile = fopen(chunkFileName, "rb");
        if (chunkFile == NULL){
            perror("Error opening chunk file");
            break;
        }
        char chunk[CHUNK_SIZE];
        size_t bytesRead;
        while ((bytesRead = fread(chunk, 1, sizeof(chunk), chunkFile)) > 0)
            fwrite(chunk, 1, bytesRead, outputFile);
        fclose(chunkFile);
        remove(chunkFileName);
        chunkIndex++;
        snprintf(chunkFileName, sizeof(chunkFileName), "%s.%03d", filePath, chunkIndex);
    }
    fclose(outputFile);
}
```
Pada fungsi ini chunk akan digabungkan. Nama chunk dibuat dengan menggabungkan filePath dan chunkIndex lalu disimpan di chunkFileName. Kemudian dilakukan looping untuk mengecek apakah file chunk ada. Jika ada akan dibaca dan ditulis ke outputFile. chunkIndex akan ditambah 1 lalu dibuat nama file berikutnya berdasarkan filePath dan chunkIndex baru.

Cara menjalankan
![image](/uploads/4ba13e03b95b5d933e53abcce54f64c3/image.png)
