#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LINE_LENGTH 1024

typedef struct {
    int ID;
    char Name[50];
    int Age;
    char Photo[200];
    char Nationality[100];
    char Flag[200];
    int Overall;
    int Potential;
    char Club[200];
    char ClubLogo[200];
    char Value[50];
    char Wage[50];
    int Special;
    char PreferredFoot[50];
    float InternationalReputation;
    float WeakFoot;
    float SkillMoves;
    char WorkRate[100];
    char BodyType[100];
    char RealFace[100];
    char position[100];
    char Joined[100];
    char LoanedFrom[100];
    int ContractValidUntil;
    char Height[10];
    char Weight[10];
    char ReleaseClause[20];
    float KitNumber;
    char BestOverallRating[10];
} Player;

void printPlayer() {
    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char header[MAX_LINE_LENGTH];
    fgets(header, sizeof(header), file);

    char line[MAX_LINE_LENGTH];
    int num=0;
    while (fgets(line, sizeof(line), file)) {
        Player player;

        char *token = strtok(line, ",");
        int field = 0;
        while (token != NULL) {
            switch (field) {
                case 0:
                    player.ID = atoi(token);
                    break;
                case 1:
                    strncpy(player.Name, token, sizeof(player.Name));
                    break;
                case 2:
                    player.Age = atoi(token);
                    break;
                case 3:
                    strncpy(player.Photo, token, sizeof(player.Photo));
                    break;
                case 4:
                    strncpy(player.Nationality, token, sizeof(player.Nationality));
                    break;
                case 5:
                    strncpy(player.Flag, token, sizeof(player.Flag));
                    break;
                case 6:
                    player.Overall = atoi(token);
                    break;
                case 7:
                    player.Potential = atoi(token);
                    break;
                case 8:
                    strncpy(player.Club, token, sizeof(player.Club));
                    break;
                case 9:
                    strncpy(player.ClubLogo, token, sizeof(player.ClubLogo));
                    break;
                case 10:
                    strncpy(player.Value, token, sizeof(player.Value));
                    break;
                case 11:
                    strncpy(player.Wage, token, sizeof(player.Wage));
                    break;
                case 12:
                    player.Special = atoi(token);
                    break;
                case 13:
                    strncpy(player.PreferredFoot, token, sizeof(player.PreferredFoot));
                    break;
                case 14:
                    player.InternationalReputation = atof(token);
                    break;
                case 15:
                    player.WeakFoot = atof(token);
                    break;
                case 16:
                    player.SkillMoves = atof(token);
                    break;
                case 17:
                    strncpy(player.WorkRate, token, sizeof(player.WorkRate));
                    break;
                case 18:
                    strncpy(player.BodyType, token, sizeof(player.BodyType));
                    break;
                case 19:
                    strncpy(player.RealFace, token, sizeof(player.RealFace));
                    break;
                case 20:
                    strncpy(player.position, token, sizeof(player.position));
                    break;
                case 21:
                    strncpy(player.Joined, token, sizeof(player.Joined));
                    break;
                case 22:
                    strncpy(player.LoanedFrom, token, sizeof(player.LoanedFrom));
                    break;
                case 23:
                    player.ContractValidUntil = atoi(token);
                    break;
                case 24:
                    strncpy(player.Height, token, sizeof(player.Height));
                    break;
                case 25:
                    strncpy(player.Weight, token, sizeof(player.Weight));
                    break;
                case 26:
                    strncpy(player.ReleaseClause, token, sizeof(player.ReleaseClause));
                    break;
                case 27:
                    player.KitNumber = atof(token);
                    break;
                case 28:
                    strncpy(player.BestOverallRating, token, sizeof(player.BestOverallRating));
                    break;
            }

            token = strtok(NULL, ",");
            field++;
        }

        if (player.Age < 25 && player.Potential > 85 && strcmp(player.Club, "Manchester City") != 0) {
            num++;
            printf("Player %d\n", num);
            printf("Name: %s\n", player.Name);
            printf("Club: %s\n", player.Club);
            printf("Age: %d\n", player.Age);
            printf("Potential: %d\n", player.Potential);
            printf("Photo URL: %s\n", player.Photo);
            printf("\n");
        }
    }

    fclose(file);

}

int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    printPlayer();
    return 0;
}
